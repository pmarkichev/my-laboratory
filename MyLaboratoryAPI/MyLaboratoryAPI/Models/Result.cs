﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLaboratoryAPI.Models
{
    public class Result
    {
        public ExperimentViewModel Experiment { get; set; }
        public double[] ZeroLevel { get; set; }
        public double[] VarietyInterval { get; set; }
        public double[] RegressionCoef { get; set; }
        public double[] BAndDelta { get; set; }
        public double[] Ki { get; set; }
        public double[] NewVarietyInterval { get; set; }
        public double[] FloatNewVarietyInterval { get; set; }
        public double[][] GradientTable { get; set; }
        public string LinearModel { get; set; }
    }

    public class ExperimentViewModel
    {
        public int Id { get; set; }
        public string WorkerName { get; set; }
        public string Name { get; set; }
        public double? MinRegressionCoef { get; set; }
        public DateTime Date { get; set; }
        public string TechProcName { get; set; }
        public string TechProcDesc { get; set; }
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public double NewVarietyIntervalForMain { get; set; }
        public List<Parameter> Parameters { get; set; }
    }
}
