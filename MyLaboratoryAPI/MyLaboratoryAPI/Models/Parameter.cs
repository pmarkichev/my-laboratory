﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLaboratoryAPI.Models
{
    public class Parameter
    {
        public string Name;
        public string UnitOfMeasurment;
        public bool IsMain;
        public double VarietyInterval;
        public List<double> Tries;
        public double? Scale;
    }
}
