﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBProject;
using Extreme.Statistics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyLaboratoryAPI.Helpers;
using MyLaboratoryAPI.Models;
using Newtonsoft.Json;

namespace MyLaboratoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private readonly MyLaboratoryContext _context;

        private readonly CohrenCoef[] CohrenCoeffs = new CohrenCoef[] {
            new CohrenCoef() {YAxis = 4, XAxis = 1, Value = 0.9065 },
            new CohrenCoef() { YAxis = 8, XAxis = 2, Value = 0.5157},
            new CohrenCoef() { YAxis = 16, XAxis = 3, Value = 0.2758 },
            new CohrenCoef() { YAxis = 32, XAxis = 4, Value = 0.1377 },
            new CohrenCoef() { YAxis = 64, XAxis = 5, Value = 0.0682 },
            new CohrenCoef() { YAxis = 128, XAxis = 6, Value = 0.0337 }
        };

        private readonly CohrenCoef[] FisherCoeffs = new CohrenCoef[] {
            new CohrenCoef() {XAxis = 1, YAxis = 1, Value = 161.45 },
            new CohrenCoef() { XAxis = 4, YAxis = 2, Value = 19.247},
            new CohrenCoef() { XAxis = 11, YAxis = 3, Value = 8.76 },
            new CohrenCoef() { XAxis = 26, YAxis = 4, Value = 5.768 },
            new CohrenCoef() { XAxis = 57, YAxis = 5, Value = 4.432 },
            new CohrenCoef() { XAxis = 120, YAxis = 6, Value = 3.7047 }
        };

        private readonly StudentCoef[] StudentCoefs = new StudentCoef[] {
            new StudentCoef() { XAxis = 1, Value = 12.72 },
            new StudentCoef() { XAxis = 2, Value = 4.3},
            new StudentCoef() { XAxis = 3, Value = 3.18},
            new StudentCoef() { XAxis = 4, Value = 2.78},
            new StudentCoef() { XAxis = 5, Value = 2.57},
            new StudentCoef() { XAxis = 6, Value = 2.45}
        };

        public ValuesController(MyLaboratoryContext Context)
        {
            _context = Context;
        }
        // GET api/values
        [HttpGet("/api/getExperimentResult")]
        public ActionResult<Result> Get([FromQuery]int id)
        {
            ExperimentViewModel experiment = getExperimentViewModelById(id);

            ExperimentResponseModel experimentResult = getExperimentResult(experiment, new RequestMode[0]);
            CreateGradYIfNoExist(experimentResult.Result.GradientTable.Length, experimentResult.Result.ZeroLevel.Length, experiment.Id);

            return experimentResult.Result;
        }

        // GET api/values
        [HttpGet("/api/getGradY")]
        public ActionResult<int[][]> GetGradY([FromQuery]int id)
        {
            GradY gradY = _context.GradY.First(m => m.ExperimentId == id);



            return JsonConvert.DeserializeObject<int[][]>(gradY.GradYArray);
        }

        private ExperimentViewModel getExperimentViewModelById(int id)
        {
            #region
            var fakeparameters = new List<Parameter>();
            fakeparameters.Add(new Parameter()
            {
                Name = "x1",
                UnitOfMeasurment = "d",
                IsMain = false,
                Tries = new List<double>() { 18, 20, 22, 24, 26, 28, 30, 32 }
            });
            fakeparameters.Add(new Parameter()
            {
                Name = "x2",
                UnitOfMeasurment = "d",
                IsMain = false,
                Tries = new List<double>() { 100, 120, 140, 160, 180, 200, 220, 240 }
            });
            fakeparameters.Add(new Parameter()
            {
                Name = "x3",
                UnitOfMeasurment = "d",
                IsMain = false,
                Tries = new List<double>() { 450, 500, 550, 600, 650, 700, 750, 800 }
            });
            fakeparameters.Add(new Parameter()
            {
                Name = "y1",
                UnitOfMeasurment = "d",
                IsMain = true,
                Tries = new List<double>() { 72, 80, 95, 120, 110, 100, 91, 87 }
            });
            fakeparameters.Add(new Parameter()
            {
                Name = "y2",
                UnitOfMeasurment = "d",
                IsMain = true,
                Tries = new List<double>() { 70, 81, 93, 117, 108, 103, 93, 85 }
            });
            fakeparameters.Add(new Parameter()
            {
                Name = "y3",
                UnitOfMeasurment = "d",
                IsMain = true,
                Tries = new List<double>() { 74, 80, 94, 120, 107, 101, 92, 86 }
            });
            #endregion
            var experiment = _context.Experiment.Include(m => m.Worker).Include(m => m.Techproc).Include(m => m.Product).FirstOrDefault(m => m.Id == id);
            var parameters = _context.Techprocset.Include(m => m.Tries).Where(m => m.ExperimentId == id).Select(m => new Parameter {
                IsMain = m.IsMain,
                Name = m.Name,
                Scale = m.Scale,
                UnitOfMeasurment = m.Uom,
                VarietyInterval = m.VarietyInterval,
                Tries = m.Tries.OrderBy(n => n.Try).Select(n => n.Value).ToList()
            }).ToList();
            var experimentsViewModel = new ExperimentViewModel
            {
                Id = experiment.Id,
                Date = experiment.Date,
                Name = experiment.Name,
                ProductName = experiment.Product.Name,
                TechProcName = experiment.Techproc.Name,
                WorkerName = experiment.Worker.Name,
                MinRegressionCoef = experiment.MinRegressionCoef,
                NewVarietyIntervalForMain = experiment.NewVarietyIntervalForMain,
                ProductDesc = experiment.Product.Description,
                TechProcDesc = experiment.Techproc.Description,
                Parameters = parameters
            };
            return experimentsViewModel;
        }


        private ExperimentResponseModel getExperimentResult(ExperimentViewModel value, RequestMode[] checks)
        {
            var mainParameters = value.Parameters.Where(m => m.IsMain).ToList();
            var xParameters = value.Parameters.Where(m => !m.IsMain).ToList();
            var triesCount = mainParameters.First().Tries.Count;


            //считаем среднее по попыткам y
            var averageYByTries = new double[triesCount];
            foreach (var parameter in mainParameters)
            {
                for (var i = 0; i < triesCount; i++)
                {
                    averageYByTries[i] += parameter.Tries[i];
                }
            }

           
            averageYByTries = averageYByTries.Select(m => m / mainParameters.Count).ToArray();

            var averageX = xParameters.Select(m => m.Tries.Average()).ToArray();

            //считаем дисперсию опытов
            var dispersion = new double[triesCount];

            for (int i = 0; i < triesCount; i++)
            {
                dispersion[i] = MathHelper.CountDispersion(mainParameters.Select(m => m.Tries[i]), averageYByTries[i]);
            }


            //число степеней свободы каждой оценки
            var fu = mainParameters.Count - 1;

            //табличный коэффициент Кохрена
            var cohrenCoefFromTable = CohrenCoeffs.Where(m => m.YAxis == triesCount && m.XAxis == fu).First().Value;

            //Не табличный коэффициент Кохрена
            var cohrenCoef = dispersion.Max() / dispersion.Sum();

            //Воспроизводимость опытов
            var expReproducibility = (cohrenCoef < cohrenCoefFromTable);

            if (!expReproducibility && checks.Contains(RequestMode.CheckForRegress))
            {
                return new ExperimentResponseModel() {Result = null, CheckResult = new CheckResult() { Status = 0, Message = "Опыты не прошли проверку на воспроизводимость" } };
            } 

            //Определение дисперсии воспроизводимости
            var dispersionAverage = dispersion.Average();

            var recommendTriesCount = Convert.ToInt32(Math.Pow(2, mainParameters.Count));

            var linearModelMatrix = new List<int[]>();
            linearModelMatrix = MathHelper.GetLinearModelMatrix(mainParameters.Count);


            int[][] basicCoeffs = MathHelper.GetBasicCoeffs(mainParameters.Count, triesCount);
            double[] regressionCoeffs = MathHelper.GetRegressionCoeffs(basicCoeffs, linearModelMatrix, averageYByTries);
            string linearModel = MathHelper.GetLinearModel(basicCoeffs, linearModelMatrix, averageYByTries);

            var studentCoef = StudentCoefs.FirstOrDefault(m => m.XAxis == fu).Value;
            double bi;
            if (value.MinRegressionCoef == null)
            {
                bi = studentCoef * (Math.Sqrt(dispersion.Average()) / Math.Sqrt(triesCount));
            }
            else
            {
                bi = value.MinRegressionCoef.Value;
            }

            //меняем незначимые коэффициенты нулями
            var checkedRegressionCoeffs = regressionCoeffs.Select(m => (m > bi) ? m : 0).ToArray();
            // y рассчетные
            var countedY = MathHelper.GetCountedY(basicCoeffs, linearModelMatrix, checkedRegressionCoeffs);

            var fad = triesCount - mainParameters.Count - 1;

            // дисперсия адекватности
            var normalDispersion = averageYByTries.Select((m, index) => Math.Pow((m - countedY[index]), 2)).Sum() / fad;

            var fisherCoef = FisherCoeffs.FirstOrDefault(m => m.XAxis == fad && m.YAxis == fu).Value;
            // адекватна ли линейная модель?
            bool isLinearModelNormal = (normalDispersion / dispersion.Average()) < fisherCoef;

            if (!expReproducibility && checks.Contains(RequestMode.CheckLinearModel))
            {
                return new ExperimentResponseModel() { Result = null, CheckResult = new CheckResult() { Status = 1, Message = "Линейная модель не прошла проверку на адекватность" } };
            }

            

            Result result = new Result();
            result.Experiment = value;
            result.LinearModel = linearModel;
            result.ZeroLevel = averageX;
            result.VarietyInterval = value.Parameters.Where(m => !m.IsMain).Select(m => m.VarietyInterval).ToArray();           
            result.RegressionCoef = regressionCoeffs.Skip(1).Take(xParameters.Count).ToArray();
            result.BAndDelta = result.VarietyInterval.Select((m, index) => m * result.RegressionCoef[index]).ToArray();
            if (checks.Contains(RequestMode.CheckForNewVarietyIntervalForMain))
            {
                return new ExperimentResponseModel() { Result = null, CheckResult = new CheckResult() { Status = 2, Message = "Введите новый интервал варьирования для параметра '" + value.Parameters[result.BAndDelta.ToList().IndexOf(result.BAndDelta.Max())].Name + "'" } };
            }
            result.Ki = result.BAndDelta.Select(m => m / result.BAndDelta.Max()).ToArray();
            result.NewVarietyInterval = result.Ki.Select(m => m * value.NewVarietyIntervalForMain).ToArray();
            result.FloatNewVarietyInterval = result.NewVarietyInterval.Select((m, index) =>  (value.Parameters[index].Scale != null && Math.Round(m, 1, MidpointRounding.AwayFromZero) < value.Parameters[index].Scale.Value) ? value.Parameters[index].Scale.Value : Math.Round(m, 1, MidpointRounding.AwayFromZero)).ToArray();
            result.FloatNewVarietyInterval = result.FloatNewVarietyInterval.Select(m => m == 0 ? 0.1 : m).ToArray();
            result.GradientTable = getGradientTable(xParameters.Select(m => m.Tries.First()).ToArray(), xParameters.Select(m => m.Tries.Last()).ToArray(), result.FloatNewVarietyInterval);          
            return new ExperimentResponseModel() { Result = result, CheckResult = new CheckResult() { Status = 3, Message = "Всё гуд" } }; ;
        }

        private void CreateGradYIfNoExist(int length, int count, int experimentId)
        {
            if (!_context.GradY.Any(m => m.ExperimentId == experimentId))
            {
                int[][] gradYArray = new int[length][];
                for (int i = 0; i < length; i++)
                {
                    gradYArray[i] = new int[count + 1];
                }
                string gradYArrayJson = JsonConvert.SerializeObject(gradYArray);
                var gradY = new GradY { ExperimentId = experimentId, GradYArray = gradYArrayJson };
                _context.GradY.Add(gradY);
                _context.SaveChanges();
            }
          
        }

        private double[][] getGradientTable(double[] start, double[] finish, double[] floatNewVarietyInterval)
        {
            List<double[]> result = new List<double[]>();
            var temp = new double[start.Count()];
            temp = start;
            bool allMax = false;
            while (!allMax) {
                result.Add(temp);
                temp = temp.Select((m, index) => (m + floatNewVarietyInterval[index]) > finish[index] ? finish[index] : m + floatNewVarietyInterval[index]).ToArray();
                allMax = !temp.Where((m, index) => m < finish[index]).Any();
            }
            return result.ToArray();
        }


        // POST api/doExperiment 
        [HttpPost("/api/saveGradY")]
        public ActionResult SaveGradY([FromBody] int[][] value, [FromQuery] int id)
        {
            var gradY = _context.GradY.First(m => m.ExperimentId == id);
            gradY.GradYArray = JsonConvert.SerializeObject(value);
            _context.SaveChanges();
            return Ok();
        }

        // POST api/doExperiment 
        [HttpPost("/api/doExperiment")]
        public ActionResult<ExperimentResponseModel> DoExperiment([FromBody] ExperimentRequestModel value)
        {
            if (!value.NeededChecks.Any())
            {
                var exp = saveExperimentToDb(value.Experiment);
                value.Experiment.Id = exp.Id;
            }            
  
            ExperimentResponseModel experimentResult = getExperimentResult(value.Experiment, value.NeededChecks);
            if (experimentResult.CheckResult.Status == 3)
            {
                CreateGradYIfNoExist(experimentResult.Result.GradientTable.Length, experimentResult.Result.ZeroLevel.Length, value.Experiment.Id);
            }
            return experimentResult;
        }

        private Experiment saveExperimentToDb(ExperimentViewModel value)
        {
            var techProc = new Techproc()
            {
                Name = value.TechProcName,
                Description = value.TechProcDesc
            };
            _context.Techproc.Add(techProc);
            var product = new Product()
            {
                Name = value.ProductName,
                Description = value.ProductDesc,
            };
            _context.Product.Add(product);
            var worker = new Workers
            {
                Name = value.WorkerName,
                SecondName = "",
                Surname = "",
                Position = "Босс"
            };
            _context.Workers.Add(worker);
            _context.SaveChanges();

            var exp = new Experiment();
            exp.Name = value.Name;
            exp.Date = value.Date;
            exp.WorkerId = worker.Id;
            exp.TechProcId = techProc.Id;
            exp.ProductId = product.Id;
            exp.NewVarietyIntervalForMain = value.NewVarietyIntervalForMain;
            _context.Experiment.Add(exp);
            _context.SaveChanges();
            var parsed = value.Parameters.Select(m => new Techprocset()
            {
                ExperimentId = exp.Id,
                VarietyInterval = m.VarietyInterval,
                IsMain = m.IsMain,
                Maxvalue = 100,
                Minvalue = 0,
                Name = m.Name,
                Uom = m.UnitOfMeasurment,
                Tries = m.Tries.Select((t, index) => new Tries { Try = index + 1, Value = t }).ToList(),
                Scale = m.Scale,
            });
            _context.Techprocset.AddRange(parsed);
            _context.SaveChanges();
            return exp;
        }

        // GET api/values/5 
        [HttpGet("/api/getExperimentList")]
        public ActionResult<List<ExperimentViewModel>> GetExperimentList(int id)
        {
            var experiments = _context.Experiment.Include(m => m.Worker).Include(m => m.Techproc).Include(m => m.Product).ToList();
            var experimentsViewModel = experiments.Select(m => new ExperimentViewModel
            {
                Id = m.Id,
                Date = m.Date,
                Name = m.Name,
                ProductName = m.Product.Name,
                TechProcName = m.Techproc.Name,
                WorkerName = m.Worker.Name,
            }).ToList();
            return experimentsViewModel;
        }
    }
}
