﻿namespace MyLaboratoryAPI.Controllers
{
    internal class StudentCoef
    {
        public int XAxis { get; set; }
        public double Value { get; set; }
    }
}