﻿using MyLaboratoryAPI.Models;

namespace MyLaboratoryAPI.Controllers
{
    public class ExperimentResponseModel
    {
        public Result Result;
        public CheckResult CheckResult;
    }

    public class CheckResult
    {
        public int Status; 
        public string Message;
    }
}