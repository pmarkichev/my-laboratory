﻿using MyLaboratoryAPI.Models;

namespace MyLaboratoryAPI.Controllers
{
    public class ExperimentRequestModel
    {
        public RequestMode[] NeededChecks;
        public ExperimentViewModel Experiment;
    }

    public enum RequestMode
    {
        CheckForRegress = 0,
        CheckLinearModel = 1,
        CheckForNewVarietyIntervalForMain = 2,
    }
}