﻿namespace MyLaboratoryAPI.Controllers
{
    internal class CohrenCoef
    {
        public int YAxis;
        public int XAxis;
        public double Value;
    }
}