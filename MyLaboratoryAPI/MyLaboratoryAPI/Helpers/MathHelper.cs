﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyLaboratoryAPI.Helpers
{
    public class MathHelper
    {
        public static double CountDispersion(IEnumerable<double> tryTries, double average)
        {
            double upExpr = 0;
            foreach (var item in tryTries)
            {
                upExpr += Math.Pow((item - average), 2);
            }
            upExpr = upExpr / (tryTries.ToList().Count - 1);

            return upExpr;
        }

        public static bool checkItemForUnical(IEnumerable<int> item, List<int[]> result)
        {
            if (!item.SequenceEqual(item.Distinct()))
            {
                return false;
            }
            if (result.Any(m => m.ToHashSet().SetEquals(item.ToHashSet())))
            {
                return false;
            }
            return true;
        }

        public static double[] GetRegressionCoeffs(int[][] basicCoeffs, List<int[]> linearModelMatrix, double[] averageY)
        {
            List<double> result = new List<double>();
            result.Add(averageY.Average());
            foreach (var row in linearModelMatrix)
            {
                double coef = 0;
                foreach (var avg in averageY.Select((value, i) => new { i, value }))
                {
                    coef = coef + avg.value * basicCoeffs[avg.i].Where((m, index) => row.Contains(index)).Aggregate((x, y) => x * y);
                }
                coef = coef / averageY.Count();
                result.Add(coef);
            }
            return result.ToArray();
        }

        public static int[][] GetBasicCoeffs(int xCount, int triesCount)
        {
            int[][] result = new int[triesCount][];
            for (int i = 0; i < triesCount; i++)
            {
                string binary = Convert.ToString(i, 2).PadLeft(xCount);
                result[i] = new int[xCount];
                for (int j = binary.Length - 1; j >= 0; j--)
                {
                    result[i][binary.Length - 1 - j] = (binary[j] == '1') ? 1 : -1;
                }
            }
            return result;
        }

        public static string GetLinearModel(int[][] basicCoeffs, List<int[]> linearModelMatrix, double[] averageY)
        {
            string result = "";
            string[] input = generateXVariableNames(averageY.Count());
            double zeroCoef = 0;
            foreach (var avg in averageY.Select((value, i) => new { i, value }))
            {
                zeroCoef = zeroCoef + avg.value;
            }
            zeroCoef = zeroCoef / averageY.Count();
            result = result + zeroCoef.ToString("0.###");
            foreach (var row in linearModelMatrix)
            {
                double coef = 0;
                foreach (var avg in averageY.Select((value, i) => new { i, value }))
                {
                    coef = coef + avg.value * basicCoeffs[avg.i].Where((m, index) => row.Contains(index)).Aggregate((x, y) => x * y);
                }
                coef = coef / averageY.Count();
                result = result + ((coef>0) ? "+" : "") + coef.ToString("0.###") + input.Where((m, index) => row.Contains(index)).Aggregate((x, y) => x + y);
            }
            return result;
        }

        private static string[] generateXVariableNames(int count)
        {
            List<string> result = new List<string>();
            for (int i = 1; i <= count; i++)
            {
                result.Add(String.Format("x_{0}", i));
            }
            return result.ToArray();
        }

        public static List<int[]> GetLinearModelMatrix(int count)
        {
            int[] input = generateXIndexes(count);
            List<int[]> result = new List<int[]>();
            int i = 1;
            foreach (var item in input)
            {
                result.Add(new int[] { item });
            }
            while (i <= count)
            {
                foreach (var row in result.ToList())
                {
                    foreach (var index in input)
                    {
                        var oldItem = row.ToList();
                        oldItem.Add(index);
                        var item = oldItem.ToArray();
                        if (checkItemForUnical(item, result))
                        {
                            result.Add(item);
                        }
                    }
                }
                i++;
            };
            return result;
        }

        private static int[] generateXIndexes(int count)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < count; i++)
            {
                result.Add(i);
            }
            return result.ToArray();
        }

        internal static double[] GetCountedY(int[][] basicCoeffs, List<int[]> linearModelMatrix, double[] checkedRegressionCoeffs)
        {
            List<double> result = new List<double>();
            foreach (var row in basicCoeffs.Select((value, i) => new { i, value })) 
            {
                double coef = checkedRegressionCoeffs[0];
                foreach (var item in linearModelMatrix.Select((v, index) => new { index, v }))
                {
                    coef = coef + checkedRegressionCoeffs[item.index + 1] * row.value.Where((m, bi) => item.v.Contains(bi)).Aggregate((x, y) => x * y);
                }
                result.Add(coef);
            }
            return result.ToArray();
        }
    }
}
