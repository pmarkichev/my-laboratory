﻿using System;
using System.Collections.Generic;

namespace DBProject
{
    public partial class Techprocset
    {
        public Techprocset()
        {
            Tries = new HashSet<Tries>();
        }

        public int Id { get; set; }
        public int ExperimentId { get; set; }
        public string Name { get; set; }
        public string Specifications { get; set; }
        public double VarietyInterval { get; set; }
        public bool IsMain { get; set; }
        public decimal? Minvalue { get; set; }
        public decimal? Maxvalue { get; set; }
        public string Uom { get; set; }
        public double? Scale { get; set; }

        public virtual ICollection<Tries> Tries { get; set; }
        public virtual Experiment Experiment { get; set; }
    }
}
