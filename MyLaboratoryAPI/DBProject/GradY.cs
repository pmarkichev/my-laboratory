﻿using System;
using System.Collections.Generic;

namespace DBProject
{
    public partial class GradY
    {
        public int Id { get; set; }
        public int ExperimentId { get; set; }
        public string GradYArray { get; set; }

        public virtual Experiment Experiment { get; set; }
    }
}
