﻿using System;
using System.Collections.Generic;

namespace DBProject
{
    public partial class Tries
    {

        public int Id { get; set; }
        public int TechProcSetId { get; set; }
        public double Value { get; set; }
        public int Try { get; set; }

        public virtual Techprocset Techprocset { get; set; }
    }
}
