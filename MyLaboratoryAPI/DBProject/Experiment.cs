﻿using System;
using System.Collections.Generic;

namespace DBProject
{
    public partial class Experiment
    {

        public int Id { get; set; }
        public int WorkerId { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int TechProcId { get; set; }
        public int ProductId { get; set; }
        public double? MinRegressionCoef { get; set; }
        public double NewVarietyIntervalForMain { get; set; }

        public virtual Workers Worker { get; set; }
        public virtual Product Product { get; set; }
        public virtual Techproc Techproc { get; set; }
    }
}
