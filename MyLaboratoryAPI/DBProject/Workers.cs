﻿using System;
using System.Collections.Generic;

namespace DBProject
{
    public partial class Workers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public string Position { get; set; }

        public virtual ICollection<Experiment> Experiments { get; set; }
    }
}
