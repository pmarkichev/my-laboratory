﻿using System;
using System.Collections.Generic;

namespace DBProject
{
    public partial class Techproc
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Experiment> Experiments { get; set; }
    }  
}
