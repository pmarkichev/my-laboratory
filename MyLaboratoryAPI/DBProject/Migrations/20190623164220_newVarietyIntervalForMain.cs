﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBProject.Migrations
{
    public partial class newVarietyIntervalForMain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "NewVarietyIntervalForMain",
                table: "experiment",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NewVarietyIntervalForMain",
                table: "experiment");
        }
    }
}
