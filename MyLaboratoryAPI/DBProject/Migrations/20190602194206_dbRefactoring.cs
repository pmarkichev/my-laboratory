﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBProject.Migrations
{
    public partial class dbRefactoring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "idvalue",
                table: "value");

            migrationBuilder.RenameColumn(
                name: "idexperiment",
                table: "experiment",
                newName: "id");

            migrationBuilder.AddColumn<int>(
                name: "WorkerId",
                table: "experiment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "id",
                table: "value",
                column: "idvalue");

            migrationBuilder.CreateIndex(
                name: "IX_value_idexperiment",
                table: "value",
                column: "idexperiment");

            migrationBuilder.CreateIndex(
                name: "IX_experiment_WorkerId",
                table: "experiment",
                column: "WorkerId");

            migrationBuilder.AddForeignKey(
                name: "FK_value_experiment_idexperiment",
                table: "value",
                column: "idexperiment",
                principalTable: "experiment",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_experiment_workers_WorkerId",
                table: "experiment");

            migrationBuilder.DropForeignKey(
                name: "FK_value_experiment_idexperiment",
                table: "value");

            migrationBuilder.DropPrimaryKey(
                name: "id",
                table: "value");

            migrationBuilder.DropIndex(
                name: "IX_value_idexperiment",
                table: "value");

            migrationBuilder.DropIndex(
                name: "IX_experiment_WorkerId",
                table: "experiment");

            migrationBuilder.DropColumn(
                name: "WorkerId",
                table: "experiment");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "experiment",
                newName: "idexperiment");

            migrationBuilder.AddPrimaryKey(
                name: "idvalue",
                table: "value",
                column: "idvalue");
        }
    }
}
