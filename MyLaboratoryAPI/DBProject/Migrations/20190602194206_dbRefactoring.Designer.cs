﻿// <auto-generated />
using System;
using DBProject;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBProject.Migrations
{
    [DbContext(typeof(MyLaboratoryContext))]
    [Migration("20190602194206_dbRefactoring")]
    partial class dbRefactoring
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("Relational:Sequence:.techprocset_idsettings_seq", "'techprocset_idsettings_seq', '', '1', '1', '', '', 'Int32', 'False'");

            modelBuilder.Entity("DBProject.Experiment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<DateTime>("Date")
                        .HasColumnName("date");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name");

                    b.Property<int>("Number")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("number");

                    b.Property<int>("WorkerId");

                    b.HasKey("Id")
                        .HasName("experiment_pkey");

                    b.HasIndex("WorkerId");

                    b.ToTable("experiment");
                });

            modelBuilder.Entity("DBProject.Product", b =>
                {
                    b.Property<int>("Idproduct")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("idproduct");

                    b.Property<string>("Description")
                        .HasColumnName("description");

                    b.Property<int>("Idtechproc")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("idtechproc");

                    b.Property<string>("Name")
                        .HasColumnName("name");

                    b.HasKey("Idproduct")
                        .HasName("product_pkey");

                    b.HasIndex("Idtechproc");

                    b.ToTable("product");
                });

            modelBuilder.Entity("DBProject.Techproc", b =>
                {
                    b.Property<int>("Idtechproc")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("idtechproc");

                    b.Property<string>("Description")
                        .HasColumnName("description");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name");

                    b.HasKey("Idtechproc")
                        .HasName("techproc_pkey");

                    b.ToTable("techproc");
                });

            modelBuilder.Entity("DBProject.Techprocset", b =>
                {
                    b.Property<int>("Idtechprocset")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("idtechprocset");

                    b.Property<int>("Idtechproc")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("idtechproc")
                        .HasDefaultValueSql("nextval('techprocset_idsettings_seq'::regclass)");

                    b.Property<decimal?>("Maxvalue")
                        .HasColumnName("maxvalue")
                        .HasColumnType("numeric(4,0)");

                    b.Property<decimal?>("Minvalue")
                        .HasColumnName("minvalue")
                        .HasColumnType("numeric(4,0)");

                    b.Property<string>("Name")
                        .HasColumnName("name");

                    b.Property<string>("Specifications")
                        .HasColumnName("specifications");

                    b.Property<string>("Uom")
                        .HasColumnName("uom");

                    b.HasKey("Idtechprocset")
                        .HasName("techprocset_pkey");

                    b.HasIndex("Idtechproc");

                    b.ToTable("techprocset");
                });

            modelBuilder.Entity("DBProject.Value", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnName("idvalue");

                    b.Property<int>("ExperimentId")
                        .HasColumnName("idexperiment");

                    b.Property<int>("Idtechprocset")
                        .HasColumnName("idtechprocset");

                    b.Property<int>("Try")
                        .HasColumnName("try");

                    b.Property<decimal>("Value1")
                        .HasColumnName("value")
                        .HasColumnType("numeric");

                    b.HasKey("Id")
                        .HasName("id");

                    b.HasIndex("ExperimentId");

                    b.HasIndex("Idtechprocset");

                    b.ToTable("value");
                });

            modelBuilder.Entity("DBProject.Workers", b =>
                {
                    b.Property<int>("Idworker")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("idworker");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Position")
                        .IsRequired();

                    b.Property<string>("SecondName")
                        .IsRequired()
                        .HasColumnName("Second name");

                    b.Property<string>("Surname")
                        .IsRequired();

                    b.HasKey("Idworker")
                        .HasName("workers_pkey");

                    b.ToTable("workers");
                });

            modelBuilder.Entity("DBProject.Experiment", b =>
                {
                    b.HasOne("DBProject.Workers", "Worker")
                        .WithMany("Experiments")
                        .HasForeignKey("WorkerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DBProject.Product", b =>
                {
                    b.HasOne("DBProject.Techproc", "IdtechprocNavigation")
                        .WithMany("Product")
                        .HasForeignKey("Idtechproc")
                        .HasConstraintName("idtechproc");
                });

            modelBuilder.Entity("DBProject.Techprocset", b =>
                {
                    b.HasOne("DBProject.Techproc", "IdtechprocNavigation")
                        .WithMany("Techprocset")
                        .HasForeignKey("Idtechproc")
                        .HasConstraintName("idtechproc");
                });

            modelBuilder.Entity("DBProject.Value", b =>
                {
                    b.HasOne("DBProject.Experiment", "Experiment")
                        .WithMany("Values")
                        .HasForeignKey("ExperimentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DBProject.Techprocset", "IdtechprocsetNavigation")
                        .WithMany("Value")
                        .HasForeignKey("Idtechprocset")
                        .HasConstraintName("idtechprocset");
                });
#pragma warning restore 612, 618
        }
    }
}
