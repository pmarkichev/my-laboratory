﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBProject.Migrations
{
    public partial class AddScale : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Scale",
                table: "techprocset",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Scale",
                table: "techprocset");
        }
    }
}
