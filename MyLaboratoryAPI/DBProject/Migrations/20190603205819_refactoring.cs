﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBProject.Migrations
{
    public partial class refactoring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "idtechproc",
                table: "product");

            migrationBuilder.DropForeignKey(
                name: "idtechproc",
                table: "techprocset");

            migrationBuilder.DropForeignKey(
                name: "FK_value_experiment_idexperiment",
                table: "value");

            migrationBuilder.DropForeignKey(
                name: "idtechprocset",
                table: "value");

            migrationBuilder.DropIndex(
                name: "IX_value_idexperiment",
                table: "value");

            migrationBuilder.DropPrimaryKey(
                name: "techprocset_pkey",
                table: "techprocset");

            migrationBuilder.DropIndex(
                name: "IX_techprocset_idtechproc",
                table: "techprocset");

            migrationBuilder.DropIndex(
                name: "IX_product_idtechproc",
                table: "product");

            migrationBuilder.DropColumn(
                name: "idexperiment",
                table: "value");

            migrationBuilder.DropColumn(
                name: "idtechprocset",
                table: "techprocset");

            migrationBuilder.DropColumn(
                name: "idtechproc",
                table: "product");

            migrationBuilder.RenameColumn(
                name: "idworker",
                table: "workers",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "idvalue",
                table: "value",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "idtechprocset",
                table: "value",
                newName: "techprocsetid");

            migrationBuilder.RenameIndex(
                name: "IX_value_idtechprocset",
                table: "value",
                newName: "IX_value_techprocsetid");

            migrationBuilder.RenameColumn(
                name: "idtechproc",
                table: "techprocset",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "idtechproc",
                table: "techproc",
                newName: "id");

            migrationBuilder.AddColumn<int>(
                name: "ExperimentId",
                table: "techprocset",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "MinRegressionCoef",
                table: "experiment",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "experiment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TechProcId",
                table: "experiment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "techprocset_pkey",
                table: "techprocset",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_techprocset_ExperimentId",
                table: "techprocset",
                column: "ExperimentId");

            migrationBuilder.AddForeignKey(
                name: "FK_techprocset_experiment_ExperimentId",
                table: "techprocset",
                column: "ExperimentId",
                principalTable: "experiment",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "techprocsetid",
                table: "value",
                column: "techprocsetid",
                principalTable: "techprocset",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_techprocset_experiment_ExperimentId",
                table: "techprocset");

            migrationBuilder.DropForeignKey(
                name: "techprocsetid",
                table: "value");

            migrationBuilder.DropPrimaryKey(
                name: "techprocset_pkey",
                table: "techprocset");

            migrationBuilder.DropIndex(
                name: "IX_techprocset_ExperimentId",
                table: "techprocset");

            migrationBuilder.DropColumn(
                name: "ExperimentId",
                table: "techprocset");

            migrationBuilder.DropColumn(
                name: "MinRegressionCoef",
                table: "experiment");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "experiment");

            migrationBuilder.DropColumn(
                name: "TechProcId",
                table: "experiment");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "workers",
                newName: "idworker");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "value",
                newName: "idvalue");

            migrationBuilder.RenameColumn(
                name: "techprocsetid",
                table: "value",
                newName: "idtechprocset");

            migrationBuilder.RenameIndex(
                name: "IX_value_techprocsetid",
                table: "value",
                newName: "IX_value_idtechprocset");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "techprocset",
                newName: "idtechproc");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "techproc",
                newName: "idtechproc");

            migrationBuilder.AddColumn<int>(
                name: "idexperiment",
                table: "value",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "idtechprocset",
                table: "techprocset",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "idtechproc",
                table: "product",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "techprocset_pkey",
                table: "techprocset",
                column: "idtechprocset");

            migrationBuilder.CreateIndex(
                name: "IX_value_idexperiment",
                table: "value",
                column: "idexperiment");

            migrationBuilder.CreateIndex(
                name: "IX_techprocset_idtechproc",
                table: "techprocset",
                column: "idtechproc");

            migrationBuilder.CreateIndex(
                name: "IX_product_idtechproc",
                table: "product",
                column: "idtechproc");

            migrationBuilder.AddForeignKey(
                name: "idtechproc",
                table: "product",
                column: "idtechproc",
                principalTable: "techproc",
                principalColumn: "idtechproc",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "idtechproc",
                table: "techprocset",
                column: "idtechproc",
                principalTable: "techproc",
                principalColumn: "idtechproc",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_value_experiment_idexperiment",
                table: "value",
                column: "idexperiment",
                principalTable: "experiment",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "idtechprocset",
                table: "value",
                column: "idtechprocset",
                principalTable: "techprocset",
                principalColumn: "idtechprocset",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
