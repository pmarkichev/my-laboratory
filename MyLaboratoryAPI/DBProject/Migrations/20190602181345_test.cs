﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DBProject.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "techprocset_idsettings_seq");

            migrationBuilder.CreateTable(
                name: "techproc",
                columns: table => new
                {
                    idtechproc = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    name = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("techproc_pkey", x => x.idtechproc);
                });

            migrationBuilder.CreateTable(
                name: "workers",
                columns: table => new
                {
                    idworker = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    Surname = table.Column<string>(nullable: false),
                    Secondname = table.Column<string>(name: "Second name", nullable: false),
                    Position = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("workers_pkey", x => x.idworker);
                });

            migrationBuilder.CreateTable(
                name: "product",
                columns: table => new
                {
                    idproduct = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    idtechproc = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("product_pkey", x => x.idproduct);
                    table.ForeignKey(
                        name: "idtechproc",
                        column: x => x.idtechproc,
                        principalTable: "techproc",
                        principalColumn: "idtechproc",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "techprocset",
                columns: table => new
                {
                    idtechprocset = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    idtechproc = table.Column<int>(nullable: false, defaultValueSql: "nextval('techprocset_idsettings_seq'::regclass)"),
                    name = table.Column<string>(nullable: true),
                    specifications = table.Column<string>(nullable: true),
                    minvalue = table.Column<decimal>(type: "numeric(4,0)", nullable: true),
                    maxvalue = table.Column<decimal>(type: "numeric(4,0)", nullable: true),
                    uom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("techprocset_pkey", x => x.idtechprocset);
                    table.ForeignKey(
                        name: "idtechproc",
                        column: x => x.idtechproc,
                        principalTable: "techproc",
                        principalColumn: "idtechproc",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "value",
                columns: table => new
                {
                    idvalue = table.Column<int>(nullable: false),
                    idexperiment = table.Column<int>(nullable: false),
                    idtechprocset = table.Column<int>(nullable: false),
                    value = table.Column<decimal>(type: "numeric", nullable: false),
                    @try = table.Column<int>(name: "try", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("idvalue", x => x.idvalue);
                    table.ForeignKey(
                        name: "idtechprocset",
                        column: x => x.idtechprocset,
                        principalTable: "techprocset",
                        principalColumn: "idtechprocset",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "experiment",
                columns: table => new
                {
                    idexperiment = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    number = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    name = table.Column<string>(nullable: false),
                    date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("experiment_pkey", x => x.idexperiment);
                });

            migrationBuilder.CreateIndex(
                name: "IX_product_idtechproc",
                table: "product",
                column: "idtechproc");

            migrationBuilder.CreateIndex(
                name: "IX_techprocset_idtechproc",
                table: "techprocset",
                column: "idtechproc");

            migrationBuilder.CreateIndex(
                name: "IX_value_idtechprocset",
                table: "value",
                column: "idtechprocset");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "experiment");

            migrationBuilder.DropTable(
                name: "product");

            migrationBuilder.DropTable(
                name: "workers");

            migrationBuilder.DropTable(
                name: "value");

            migrationBuilder.DropTable(
                name: "techprocset");

            migrationBuilder.DropTable(
                name: "techproc");

            migrationBuilder.DropSequence(
                name: "techprocset_idsettings_seq");
        }
    }
}
