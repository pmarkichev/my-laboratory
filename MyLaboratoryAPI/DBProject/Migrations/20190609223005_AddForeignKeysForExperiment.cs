﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBProject.Migrations
{
    public partial class AddForeignKeysForExperiment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_experiment_ProductId",
                table: "experiment",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_experiment_TechProcId",
                table: "experiment",
                column: "TechProcId");

            migrationBuilder.AddForeignKey(
                name: "FK_experiment_product_ProductId",
                table: "experiment",
                column: "ProductId",
                principalTable: "product",
                principalColumn: "idproduct",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_experiment_techproc_TechProcId",
                table: "experiment",
                column: "TechProcId",
                principalTable: "techproc",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_experiment_product_ProductId",
                table: "experiment");

            migrationBuilder.DropForeignKey(
                name: "FK_experiment_techproc_TechProcId",
                table: "experiment");

            migrationBuilder.DropIndex(
                name: "IX_experiment_ProductId",
                table: "experiment");

            migrationBuilder.DropIndex(
                name: "IX_experiment_TechProcId",
                table: "experiment");
        }
    }
}
