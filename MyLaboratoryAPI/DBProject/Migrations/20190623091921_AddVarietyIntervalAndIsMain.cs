﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBProject.Migrations
{
    public partial class AddVarietyIntervalAndIsMain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsMain",
                table: "techprocset",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "VarietyInterval",
                table: "techprocset",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsMain",
                table: "techprocset");

            migrationBuilder.DropColumn(
                name: "VarietyInterval",
                table: "techprocset");
        }
    }
}
