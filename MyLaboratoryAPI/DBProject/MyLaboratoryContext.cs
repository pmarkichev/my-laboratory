﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DBProject
{
    public partial class MyLaboratoryContext : DbContext
    {
        public MyLaboratoryContext()
        {
        }

        public MyLaboratoryContext(DbContextOptions<MyLaboratoryContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Experiment> Experiment { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Techproc> Techproc { get; set; }
        public virtual DbSet<Techprocset> Techprocset { get; set; }
        public virtual DbSet<Tries> Tries { get; set; }
        public virtual DbSet<Workers> Workers { get; set; }
        public virtual DbSet<GradY> GradY { get; set; }

        // Unable to generate entity type for table 'public.research'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Database=MyLaboratory;Username=postgres;Password=QweAsd123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Experiment>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("experiment_pkey");

                entity.ToTable("experiment");

                entity.Property(e => e.Id).HasColumnName("id").ValueGeneratedOnAdd();

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.HasOne(e => e.Worker).WithMany(w => w.Experiments).HasForeignKey(e => e.WorkerId);
                entity.HasOne(e => e.Product).WithMany(w => w.Experiments).HasForeignKey(e => e.ProductId);
                entity.HasOne(e => e.Techproc).WithMany(w => w.Experiments).HasForeignKey(e => e.TechProcId);


            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("product_pkey");

                entity.ToTable("product");

                entity.Property(e => e.Id).HasColumnName("idproduct").ValueGeneratedOnAdd();

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name).HasColumnName("name");
            });
            modelBuilder.Entity<GradY>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Id");

                entity.ToTable("grady");

                entity.Property(e => e.Id).HasColumnName("Id").ValueGeneratedOnAdd();

                entity.Property(e => e.ExperimentId).HasColumnName("experimentid");

                entity.Property(e => e.GradYArray).HasColumnName("gradyarray");
            });

            modelBuilder.Entity<Techproc>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("techproc_pkey");

                entity.ToTable("techproc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Techprocset>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("techprocset_pkey");

                entity.ToTable("techprocset");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('techprocset_idsettings_seq'::regclass)");

                entity.Property(e => e.Maxvalue)
                    .HasColumnName("maxvalue")
                    .HasColumnType("numeric(4,0)");

                entity.Property(e => e.Minvalue)
                    .HasColumnName("minvalue")
                    .HasColumnType("numeric(4,0)");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Specifications).HasColumnName("specifications");

                entity.Property(e => e.Uom).HasColumnName("uom");
            });

            modelBuilder.Entity<Tries>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("id");

                entity.ToTable("value");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.TechProcSetId).HasColumnName("techprocsetid");

                entity.Property(e => e.Try).HasColumnName("try");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("numeric");
                entity.HasOne(d => d.Techprocset)
                    .WithMany(p => p.Tries)
                    .HasForeignKey(d => d.TechProcSetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("techprocsetid");
            });

            modelBuilder.Entity<Workers>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("workers_pkey");

                entity.ToTable("workers");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Position).IsRequired();

                entity.Property(e => e.SecondName)
                    .IsRequired()
                    .HasColumnName("Second name");

                entity.Property(e => e.Surname).IsRequired();
            });

            modelBuilder.HasSequence<int>("techprocset_idsettings_seq");
        }
    }
}
