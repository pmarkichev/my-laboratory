import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TopMenuComponent } from './components/top-menu/top-menu.component';
import {RouterModule, Routes} from '@angular/router';
import {MatTabsModule} from '@angular/material/tabs'
import {MatTableModule} from '@angular/material/table'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ExperimentParametersTableComponent } from './components/experiment-parameters-table/experiment-parameters-table.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatInputModule,
  MatRadioModule,
  MatSnackBarModule,
  MatSortModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ExperimentService} from './services/experiment.service';
import {HttpClientModule} from '@angular/common/http';
import { ExperimentListComponent } from './components/experiment-list/experiment-list.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ExperimentResultComponent } from './components/experiment-result/experiment-result.component';
import {MatIconModule} from '@angular/material/icon';
import {MathJaxModule} from 'ngx-mathjax';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';
import { LoginComponent } from './components/login/login.component';
import { InputModalComponent } from './modals/input-modal/input-modal.component';
import { InfoModalComponent } from './modals/info-modal/info-modal.component';

const routes: Routes = [
  { path: 'test', component: TopMenuComponent },
  { path: 'result/:id', component: ExperimentResultComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    ExperimentParametersTableComponent,
    ExperimentListComponent,
    ExperimentResultComponent,
    LoginComponent,
    InputModalComponent,
    InfoModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSortModule,
    RouterModule.forRoot(routes, { useHash: true }),
    MatTabsModule,
    MatTableModule,
    MatButtonModule,
    MathJaxModule.config({
      version: '2.7.5',
      config: 'TeX-AMS-MML_HTMLorMML',
      hostname: 'cdnjs.cloudflare.com'
    }),
    MatInputModule,
    FormsModule,
    MatCardModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatCheckboxModule,
    MatRadioModule,
    MatIconModule,
    MatDialogModule
  ],
  entryComponents: [InputModalComponent, InfoModalComponent],
  providers: [ExperimentService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
    matIconRegistry.addSvgIcon('print', domSanitizer.bypassSecurityTrustResourceUrl('./assets/baseline-print-24px.svg'));
  }
}
