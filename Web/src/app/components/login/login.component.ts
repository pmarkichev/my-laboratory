import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output()
  doLogin: EventEmitter<any> = new EventEmitter<any>();

  form = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  })


  constructor(private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  onSubmit() {
    this.doLogin.emit();
    //this.snackBar.open('Пара логин/пароль не найдена', '', { duration: 2500, verticalPosition: 'top'});
 /*   this.experimentService.doLoginRequest(this.form.value).subscribe(data => {
        this.doLogin.emit();
      }
    }, error => {
      this.form.reset();
      this.snackBar.open('Пара логин/пароль не найдена', '', { duration: 2500, verticalPosition: 'top'});
    });*/

  }

}
