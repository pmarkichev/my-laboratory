import {Component, EventEmitter, NgZone, OnInit, Output} from '@angular/core';
import {MatTableDataSource, Sort} from '@angular/material';
import {ExperimentService} from '../../services/experiment.service';
import {Router} from '@angular/router';
import {Experiment} from '../../models/experiment';

@Component({
  selector: 'app-experiment-list',
  templateUrl: './experiment-list.component.html',
  styleUrls: ['./experiment-list.component.css']
})
export class ExperimentListComponent implements OnInit {

  constructor(private service: ExperimentService,
              private zone: NgZone,
              private router: Router) { }

  displayedColumns: string[] = ['id', 'name', 'date', 'workerName', 'techProcName', 'productName'];

  data: Experiment[] = [];
  dataSource = new MatTableDataSource<Experiment>(this.data);

  @Output()
  openResult: EventEmitter<number> = new EventEmitter<number>();

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.service.getExperimentList().subscribe(m => {
      this.data= m;
      this.dataSource = new MatTableDataSource<Experiment>(this.data);
    });
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    if (!sort.active || sort.direction === '') {
      return;
    }

    this.data =this.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id': return this.compare(a.id, b.id, isAsc);
        case 'date': return this.compare(a.date, b.date, isAsc);
        case 'techProcName': return this.compare(a.techProcName, b.techProcName, isAsc);
        case 'workerName': return this.compare(a.workerName, b.workerName, isAsc);
        default: return 0;
      }
    }).slice(0);
    this.dataSource = new MatTableDataSource<Experiment>(this.data);
  }

  openResultClick(id: number) {
    this.openResult.emit(id);
  }

  compare(a: Number | string | Date, b: Number | string | Date, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
