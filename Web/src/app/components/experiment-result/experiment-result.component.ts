import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExperimentService} from '../../services/experiment.service';
import {ExperimentResult} from '../../models/experiment-result';
import {Parameter} from '../../models/experiment';
import html2canvas from 'html2canvas';
import * as jspdf from 'jspdf';


@Component({
  selector: 'app-experiment-result',
  templateUrl: './experiment-result.component.html',
  styleUrls: ['./experiment-result.component.css']
})
export class ExperimentResultComponent implements OnInit {

  @Output()
  close: EventEmitter<number> = new EventEmitter<number>();

  @Input()
  id: number;

  data: ExperimentResult;
  xparams: Parameter[];
  constNames: string[] = [];
  gradY: number[][] = [];


  constructor(private service: ExperimentService) {
  }

  ngOnInit() {
    this.service.getExperimentResult(this.id)
      .subscribe(m => {this.data = m;
      console.log(this.data);
      this.xparams = m.experiment.parameters.filter(n => !n.isMain)
        let xindex = 1;
        let yindex = 1;
        for (let item of this.data.experiment.parameters) {
          if (item.isMain) {
            this.constNames.push("y_"+ yindex);
            yindex++;
          } else {
            this.constNames.push("x_"+ xindex);
            xindex++;
          }
        };
        this.service.getGradY(this.id).subscribe(m => this.gradY = m);
      });


  }
  export() {
    var HTML_Width = document.querySelector("#report").scrollWidth;
    var HTML_Height = document.querySelector("#report").scrollHeight;
    var top_left_margin = 15;
    var PDF_Width = HTML_Width+(top_left_margin*2);
    var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;

    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
    html2canvas(document.querySelector("#report"),{allowTaint:true}).then(canvas => {
      canvas.getContext('2d');
      console.log(canvas.height+"  "+canvas.width);
      var pdf = new jspdf('p', 'pt', [PDF_Width, PDF_Height]);

      var imgData  = canvas.toDataURL("image/jpeg", 1.0);
      pdf.addImage(imgData, top_left_margin,top_left_margin,canvas_image_width, canvas_image_height);
      for (var i = 1; i <= totalPDFPages; i++) {
        pdf.addPage(PDF_Width, PDF_Height);
        console.log(-(PDF_Height*i)+(top_left_margin*4));
        pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
      }
      pdf.save('report.pdf');
    });
  }

  onCloseClick() {
    this.close.emit();
  }

  saveGradY() {
    this.service.saveGradY(this.id, this.gradY).subscribe(m => console.log("таблица градиентов сохранена!"))
  }

  getAverageFromRow(gradYElement: number[]) {
    let sum = 0;
    for( let i = 0; i < gradYElement.length-1; i++ ){
      sum += Number(gradYElement[i]);
    }
    var avg = sum/(gradYElement.length-1);
    return avg;
  }

  isRowAverageMax(index: number) {
    let avg = this.getAverageFromRow(this.gradY[index]);
    if (avg == 0) {
      return false;
    }
    return avg == Math.max(...this.gradY.map(m => this.getAverageFromRow(m)).filter(m => m))
  }
  isRowAverageMin(index: number) {
    let avg = this.getAverageFromRow(this.gradY[index]);
    if (avg == 0) {
      return false;
    }
    return avg == Math.min(...this.gradY.map(m => this.getAverageFromRow(m)).filter(m => m))
  }

}
