import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {ExperimentRequestModel, ExperimentService} from '../../services/experiment.service';
import {Experiment, Parameter} from '../../models/experiment';
import {environment} from '../../../environments/environment';
import {InputModalComponent} from '../../modals/input-modal/input-modal.component';
import {InfoModalComponent} from '../../modals/info-modal/info-modal.component';



@Component({
  selector: 'app-experiment-parameters-table',
  templateUrl: './experiment-parameters-table.component.html',
  styleUrls: ['./experiment-parameters-table.component.css']
})
export class ExperimentParametersTableComponent implements OnInit {

  experiment: Experiment = {
     parameters: []
  }

  json = '{"parameters":[{"1":"18","2":"20","3":"22","4":"24","5":"26","6":"28","7":"30","8":"32","name":"Толщина бамбука","isMain":false,"unitOfMeasurement":"мм","tries":["18","20","22","24","26","28","30","32"],"min":"18","max":"32","varietyInterval":"6"},{"1":"100","2":"120","3":"140","4":"160","5":"180","6":"200","7":"220","8":"240","name":"Толщина слоя стекловолокна","isMain":false,"unitOfMeasurement":"мкм","tries":["100","120","140","160","180","200","220","240"],"min":"100","max":"240","varietyInterval":"20"},{"1":"450","2":"500","3":"550","4":"600","5":"650","6":"700","7":"750","8":"800","name":"Усилие скручивания","isMain":false,"unitOfMeasurement":"Н","tries":["450","500","550","600","650","700","750","800"],"min":"450","max":"800","varietyInterval":"50"},{"1":"72","2":"80","3":"95","4":"120","5":"110","6":"100","7":"91","8":"87","name":"Величина отклонения","isMain":true,"unitOfMeasurement":"мм","tries":["72","80","95","120","110","100","91","87"]},{"1":"70","2":"81","3":"93","4":"117","5":"108","6":"103","7":"93","8":"85","name":"Величина отклонения","isMain":true,"unitOfMeasurement":"мм","tries":["70","81","93","117","108","103","93","85"]},{"1":"74","2":"80","3":"94","4":"120","5":"107","6":"101","7":"92","8":"86","name":"Величина отклонения","isMain":true,"unitOfMeasurement":"мм","tries":["74","80","94","120","107","101","92","86"]}],"minRegressionCoef":"","date":"2019-06-22","productName":"Композит","productDesc":"Вертикально ламинированный бамбук, укрепленный слоем стекловолокна","techProcName":"Скручивание","techProcDesc":"Продольное скручивание","workerName":"Розенберг А.А.","name":"Тестирование гибкости композита"}'

  columnNameMap: Map<string, string> = new Map<string, string>();

  displayedColumns: string[] = ['isMain', 'name', 'min', 'max', 'unitOfMeasurement', /*'1', '2', '3','4', '5', '6','7', '8',*/ "varietyInterval", "scale"];
  neededChecks: number[] = [0,1,2];
  dataSource;
  private rowIndex = 0;
  private columnIndex = 1;
  experimentDate: string;
  techProc: any;
  productName: any;
  id: number;

  @Output()
  openResult: EventEmitter<number> = new EventEmitter<number>();

  constructor(private service: ExperimentService,
              public dialog: MatDialog) {
    console.log(environment.apiUrl);
    this.experimentDate = new Date().toISOString().slice(0, 10); }

  ngOnInit() {
   //this.experiment = JSON.parse(this.json);
    this.dataSource = new MatTableDataSource<Parameter>(this.experiment.parameters);
    this.columnNameMap.set("isMain","Отклик");
    this.columnNameMap.set("name","Название");
    this.columnNameMap.set("min","Мин.*");
    this.columnNameMap.set("max","Макс.*");
    this.columnNameMap.set("unitOfMeasurement","Единицы измерения");
    this.columnNameMap.set("varietyInterval","Интервал варьирования");
    this.columnNameMap.set("scale","Цена деления прибора*");
  }

  addRow() {
    //удаляем интервал варьирования, потом добавляем назад, чтобы был в конце списка ¯\_(ツ)_/¯
    this.displayedColumns.splice(this.displayedColumns.length-2, 2);
    this.experiment.parameters.push(this.createEmptyParameter());
    this.experiment.parameters.push(this.createEmptyParameter());
    this.rowIndex++;
    if (this.rowIndex !== 1) {
      while (this.columnIndex <= Math.pow(2, this.rowIndex)) {
        this.displayedColumns.push(this.columnIndex.toString())
        this.columnIndex++;
      }
    }
    this.displayedColumns.push("varietyInterval");
    this.displayedColumns.push("scale");
    this.dataSource._updateChangeSubscription()
  }

  deleteRow() {
    this.displayedColumns.splice(this.displayedColumns.length-2, 2)
    this.experiment.parameters.splice(this.experiment.parameters.length - 2, 2);
    this.rowIndex--;
    let index = this.columnIndex - 1;
      while (index > Math.pow(2, this.rowIndex)) {
        this.displayedColumns.splice(this.displayedColumns.length - 1, 1)
        this.columnIndex--;
        index--;
    }
    this.displayedColumns.push("varietyInterval");
    this.displayedColumns.push("scale");
    this.dataSource._updateChangeSubscription()
  }

  private createEmptyParameter() {
    return {name:'', isMain: false, unitOfMeasurement:'', tries:[0]};
  }

  createExperiment() {
    this.experiment.parameters.map(m => {
      m.tries = [];
      for (var _i = 1; _i <= this.columnIndex-1; _i++) {
          m.tries.push(m[_i]);
      }
    })
    this.doExperiment(this.experiment);
  }

async doExperiment(experiment: Experiment) {
    let requestModel: ExperimentRequestModel = {neededChecks: this.neededChecks, experiment: experiment};
    let status: number = 10;
    let result;
    result = await this.service.createExperiment(requestModel).toPromise();
    status = result["checkResult"]["status"];
    console.log(status);
  if (status == 0) {
    this.neededChecks = this.neededChecks.filter(m => m != 0);
    let needToContinue = await this.dialog.open(InfoModalComponent, {
      width: '250px',
      data: {message: result["checkResult"]["message"],}
    }).afterClosed().toPromise();
    if (needToContinue) {
      requestModel = {neededChecks: this.neededChecks, experiment: experiment};
      result = await this.service.createExperiment(requestModel).toPromise();
      status = result["checkResult"]["status"];
    }
  }
    if (status == 1) {
      this.neededChecks = this.neededChecks.filter(m => m != 0 && m != 1);
      let needToContinue = await this.dialog.open(InfoModalComponent, {
        width: '250px',
        data: {message: result["checkResult"]["message"],}
      }).afterClosed().toPromise();
      if (needToContinue) {
        requestModel = {neededChecks: this.neededChecks, experiment: experiment};
        result = await this.service.createExperiment(requestModel).toPromise();
        status = result["checkResult"]["status"];
      }
    }
    if (status == 2) {
      this.neededChecks = [];
      let dialogResult = await this.dialog.open(InputModalComponent, {
        width: '250px',
        data: {message: result["checkResult"]["message"],}
      }).afterClosed().toPromise();
      console.log(dialogResult);
      if (dialogResult) {
        experiment.newVarietyIntervalForMain = dialogResult;
        requestModel = {neededChecks: this.neededChecks, experiment: experiment};
        result = await this.service.createExperiment(requestModel).toPromise();
        status = result["checkResult"]["status"];
      }
    }
    if (status == 3) {
      this.id = result["result"]["experiment"]["id"];
      this.openResult.emit(this.id);
    }
}

  getColumnName(column: string) {
    if (isNaN(column as any)) {return this.columnNameMap.get(column);}
    else return column;
  }

  productDescChange($event) {
    try {
      this.experiment.productDesc = $event.target.value;
    } catch(e) {
      console.info('could not set productDesc');
    }
  }

  techProcDescChange($event) {
    try {
      this.experiment.techProcDesc = $event.target.value;
    } catch(e) {
      console.info('could not set techProcDesc');
    }
  }
}
