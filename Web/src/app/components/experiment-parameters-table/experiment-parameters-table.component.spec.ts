import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperimentParametersTableComponent } from './experiment-parameters-table.component';

describe('ExperimentParametersTableComponent', () => {
  let component: ExperimentParametersTableComponent;
  let fixture: ComponentFixture<ExperimentParametersTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperimentParametersTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperimentParametersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
