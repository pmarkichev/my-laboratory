import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ExperimentResult} from '../models/experiment-result';
import {Experiment} from '../models/experiment';

const httpOptions = {

  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
}

export class ExperimentRequestModel {
  neededChecks: number[];
  experiment: Experiment
}

@Injectable({
  providedIn: 'root'
})
export class ExperimentService {

  constructor(private http: HttpClient) { }

  createExperiment(request: ExperimentRequestModel) {
    return this.http.post('https://localhost:44361/api/DoExperiment', request, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
  getExperimentList() {
    return this.http.get<Experiment[]>('https://localhost:44361/api/getExperimentList');
  }

  getExperimentResult(id: number) {
    return this.http.get<ExperimentResult>(`https://localhost:44361/api/getExperimentResult?id=${id}`);
  }


  getGradY(id: number) {
    return this.http.get<number[][]>(`https://localhost:44361/api/getGradY?id=${id}`);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };


  saveGradY(id: number, gradY: number[][]) {
    return this.http.post(`https://localhost:44361/api/saveGradY?id=${id}`, gradY, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}
