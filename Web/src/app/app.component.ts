import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  resultId: number;
  isResultOpen: boolean;
  isAuthentificated: boolean;

  openResult(id: number) {
    this.resultId = id
    this.isResultOpen = true
  }

  closeResult() {
    this.isResultOpen = false
  }

  doLogin() {
    this.isAuthentificated = true;
  }
}

