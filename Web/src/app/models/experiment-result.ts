import {Experiment} from './experiment';

export interface ExperimentResult {
  experiment: Experiment;
  zeroLevel: number[];
  varietyInterval: number[];
  regressionCoef: number[];
  baAndDelta: number[];
  ki: number[];
  newVarietyInterval: number[];
  floatNewVarietyInterval: number[];
  gradientTable: number[][];
  linearModel: string;
}
