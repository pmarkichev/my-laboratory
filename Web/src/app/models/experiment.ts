export interface Experiment {
  id?: number;
  name?: string;
  workerName?: string;
  date?: Date;
  techProcName?: string;
  techProcDesc?: string;
  productName?: string;
  productDesc?: string;
  newVarietyIntervalForMain?: number;
  minRegressionCoef?: number;
  parameters: Parameter[];
}

export interface Parameter {
  name: string;
  unitOfMeasurement: string;
  min?: number;
  max?: number
  isMain: boolean;
  tries: number[];
  varietyInterval?: number;
  scale?: number;
}
